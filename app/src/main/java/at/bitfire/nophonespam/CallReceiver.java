/*
 * Copyright © Ricki Hirner (bitfire web engineering).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package at.bitfire.nophonespam;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.text.TextUtilsCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.internal.telephony.ITelephony;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.security.auth.callback.Callback;

import at.bitfire.nophonespam.model.DbHelper;
import at.bitfire.nophonespam.model.Number;
import fi.johannes.tasks.UpdateTimesCalled;

public class CallReceiver extends BroadcastReceiver {
  private static final String TAG = "NoPhoneSpam";
  private static final String channelId = "default";

  private static final int NOTIFY_REJECTED = 0;

  private NotificationChannel channel = null;

  private void initChannels(Context context) {
    if (Build.VERSION.SDK_INT < 26) {
      return;
    }
    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    channel = new NotificationChannel(channelId, TAG, NotificationManager.IMPORTANCE_HIGH);
    channel.setDescription("Channel description");
    assert notificationManager != null;
    notificationManager.createNotificationChannel(channel);
  }

  private Consumer<Void> callBack = (v) -> BlacklistObserver.notifyUpdated();

  private void updateTimesCalled(DbHelper dbHelper, Number number, Consumer<Void> callback) {

    UpdateTimesCalled timesCalled = new UpdateTimesCalled(dbHelper, callback);
    ContentValues values = new ContentValues();
    values.clear();
    values.put(Number.LAST_CALL, System.currentTimeMillis());
    values.put(Number.TIMES_CALLED, number.timesCalled + 1);
    timesCalled.execute(values);
  }

  @Override
  public void onReceive(Context context, Intent intent) {
    if (TelephonyManager.ACTION_PHONE_STATE_CHANGED.equals(intent.getAction()) &&
      intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_RINGING)) {
      String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
      Log.i(TAG, "Received call: " + incomingNumber);
      // private number (no caller ID)
      if (TextUtils.isEmpty(incomingNumber) && new Settings(context).blockHiddenNumbers()) {
        rejectCall(context, null);
      }
      else {
        DbHelper dbHelper = new DbHelper(context);
        try {
          SQLiteDatabase db = dbHelper.getReadableDatabase();
          Cursor c = db.query(Number._TABLE, null, "? LIKE " + Number.NUMBER, new String[]{incomingNumber}, null, null, null);
          if (c.moveToNext()) {
            ContentValues values = new ContentValues();
            DatabaseUtils.cursorRowToContentValues(c, values);
            Number number = Number.fromValues(values);

            rejectCall(context, number);
            updateTimesCalled(dbHelper, number, callBack);
          }
          c.close();
        } finally {
          dbHelper.close();
        }
      }
    }
  }

  protected void rejectCall(@NonNull Context context, Number number) {

    TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    Class c = null;
    try {
      c = Class.forName(tm.getClass().getName());
      Method m = c.getDeclaredMethod("getITelephony");
      m.setAccessible(true);

      ITelephony telephony = (ITelephony) m.invoke(tm);
      telephony.endCall();
    } catch (Exception e) {
      e.printStackTrace();
    }

    initChannels(context);
    Settings settings = new Settings(context);
    String contentTitle = context.getString(R.string.receiver_notify_call_rejected);
    String contentText = number != null ? (number.name != null ? number.name : number.number) : context.getString(R.string.receiver_notify_private_number);
    if (settings.showNotifications()) {
      Notification notification = new NotificationCompat.Builder(context, channelId)
        .setSmallIcon(R.mipmap.ic_launcher)
        .setContentTitle(contentTitle)
        .setContentText(contentText)
        .setPriority(NotificationCompat.PRIORITY_HIGH)
        .setCategory(NotificationCompat.CATEGORY_CALL)
        .setShowWhen(true)
        .setAutoCancel(true)
        .setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, BlacklistActivity.class), PendingIntent.FLAG_UPDATE_CURRENT))
        .addPerson("tel:" + number)
        .setGroup("rejected")
        .build();

      String tag = number != null ? number.number : "private";
      NotificationManagerCompat.from(context).notify(tag, NOTIFY_REJECTED, notification);
    }

  }

}
