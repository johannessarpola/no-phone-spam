package fi.johannes.tasks;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Supplier;

import at.bitfire.nophonespam.BlacklistObserver;
import at.bitfire.nophonespam.model.DbHelper;
import at.bitfire.nophonespam.model.Number;

/**
 * Johannes on 10.11.2018.
 */
public class UpdateTimesCalled extends AsyncTask<ContentValues, Void, Void> {

  private DbHelper helper;
  private Consumer<Void> callback;

  public UpdateTimesCalled(DbHelper helper, Consumer<Void> callback) {
    this.helper = helper;
    this.callback = callback;
  }

  @Override
  protected final Void doInBackground(ContentValues... values) {
    SQLiteDatabase db = helper.getWritableDatabase();
    Arrays.stream(values).forEach(cv ->{
      Number number = Number.fromValues(cv);
      db.update(Number._TABLE, cv, Number.NUMBER + "=?", new String[]{number.number});
    });
    return null;
  }

  @Override
  protected void onPostExecute(Void aVoid) {
    callback.accept(aVoid);
    super.onPostExecute(aVoid);
  }
}
